var express = require('express'),
    passport = require('passport'),
    config = require('./config'),
    http = require('http'),
    app = express(),
    server,
    routes;

var port = 8081;

server = http.createServer(app);
config(app, passport);

routes = [
  './app/web/routes',
  './app/users/routes',
  './app/pizzas/routes',
  './app/transactions/routes',
];

routes.forEach(function(path) {
  require(path)(app, passport);
});

server.listen(port, function(err) {
  if (err) {
    console.error('Unable to listen for connections', err);
    process.exit(1);
  }

  console.log('running on port', port);
});


