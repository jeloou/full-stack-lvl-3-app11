var db = require('mongoose'),
    Pizza = db.model('Pizza');

module.exports = (function(app) {
  app.route('/pizzas')
      /*
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
      */
    .get(function(req, res){
      Pizza.fetch(function(err, pizzas) {
	if (err) return res.json(err);
	res.json(pizzas);
      });
    })
    .post(function(req, res) {
      var args,
	  pizza;

      pizza = {
	nombre: req.body.nombre,
	ingredientes : req.body.ingredientes.split(','),
	ingredientes_price : req.body.ingredientes_price,
	size: req.body.size
      };

      Pizza.add(pizza, function(err, pizza) {
	if (err) return res.status(400).json(err);
	res.json(pizza);
      })
    });

  //Retorno todas las pizzas
  //Retorna una pizza segun Id
  app.route('/pizzas/:id')
  //.all(function(req, res, next) {
  // if (!req.user) return res.status(401).end();
  // next();
  // })
    .get(function(req, res) {
      //Obtener Id de la Pizza
      var temp = {_id: req.params.id};
      Pizza.get(temp, function(err, pizza){
        if (err) return res.status(400).json(err);
        res.json(pizza);
      })
    })
    .put(function(req, res) {
      var pizza, temp;

      temp = req.body;
      pizza =  {_id: req.params.id,
            payload: temp
      };

      Pizza.change(pizza, function(err, res){
        if(err) return res.json(err);
        res.json(res);
      })

    })
});
