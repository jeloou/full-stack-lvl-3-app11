var db = require('mongoose');

var sizePrices = {
  small:  3000,
  medium:  5000,
  large:  7000,
  xl:     9000
};

var Schema = new db.Schema({
  nombre: {type: String, lowercase: true, trim: true, default: 'Pedido'},
  ingredientes: [{type: String, enum: [
    'pepperoni', 'tomate', 'cebolla', 'bacon'
  ]}],
  ingredientes_price: {type: Number},
  size: {type: String, enum:[
    'small', 'medium', 'large', 'xl'
  ], required: true}
});

Schema.methods.toJSON = function() {
  return {
    id: this._id,
    nombre: this.nombre,
    ingredientes: this.ingredientes,
    size: this.size,
    price: this.price
  };
};

Schema
  .virtual('price')
  .get(function() {
    const Calculo = (this.ingredientes_price *  this.ingredientes.length + sizePrices[this.size]);
    if (this.nombre == "pedido"){
      return Calculo;
    }else{
      return Calculo * 0.95;
    }
});

Schema.statics.add = function(payload, fn) {
  var pizza;

  pizza = payload;
  pizza = new(this)(pizza);  //Instancio el Schema y le agregamos la data
  //Es lo mismo que decir todo = new this(todo);
  //Es lo mismo que decir todo = new Schema(todo);

  pizza.save(function(err, pizza){

    if(err){
      return fn(err);
    }

    pizza.save(function(err){
      fn(null, pizza);
    })
  });
};

Schema.statics.fetch = function(fn) {
  this
    .find({})
    .exec(function(err, data){
      if (err) {
	return fn(err);
      }
      fn(null, data)
    });
};

//ACTUALIZAR LA PIZZA
Schema.statics.change = function(args, fn) {
	
  var Transaction = db.model('Transaction');  //Importo el mdoelo de Transaction 
                                              //para validar el estado de la pizza

  var pizzaId = args.id;
  var pizza   = args.payload;

Transaction.canChangeIngredients(pizzaId, function(err, PuedoModificar){

  if (!PuedoModificar){
    return;
  }
  
   this
      .findOne({_id: pizzaId, status: true})
      .exec(function(err, data){
        
          if(err){
            return fn(err);
          }

          data.nombre = pizza.nombre;
          data.ingredientes  = pizza.ingredientes;
          data.ingredientes_price  = pizza.ingredientes_price;
          data.size  = pizza.size;
          
          data.save( function (err, data) {
            
              if(err) return fn(err);
            
              fn(null, data);
          
          });
      
      });

});



 



  
};




var Pizza = db.model('Pizza', Schema);
