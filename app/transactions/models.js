var db = require('mongoose');
var ObjectId = db.Schema.ObjectId;

var TransactionSchema = new db.Schema({
  total: {type: Number},
  pizzas: [{type: ObjectId, ref: 'Pizza', required: true}],
  client: {type: ObjectId, ref: 'User', required: true},
  waiter: {type: ObjectId, ref: 'User', required: true},
  ttc: {type: ObjectId, ref: 'TTC', required: true},
  status: {type: ObjectId, ref: 'TransStatus', required: true}

})

TransactionSchema.methods.toJSON = function() {
  return {
    id: this._id,
    total: this.total,
    pizzas: this.pizzas,
    client: this.client,
    waiter: this.waiter,
    ttc: this.ttc,
    status: this.status
  };
};

TransactionSchema.statics.add = function(stuff, cb) {
  var transact = new this(stuff);
  // new Transaction(stuff)
  // [ 'f2f', 'dlv' ]
  // ['web', 'loc']
  Status.findOne({order_org: stuff.ttc[0], service: stuff.ttc[1]}, function(err, ttc) {
    transact.ttc = ttc;
    transact.save(function(err){
      if (err) {
	return cb(err);
      }
      cb(null,transact);
    });
  });
}



var TransStatusSchema = new db.Schema({
  dateTime: {type: Date, default: Date.now},
  doneTime: {type: Date, required: true, default: Date.now},
  status: {type:String, enum: ['kitchen','delivery']},
  done: {type: Boolean}
});

TransStatusSchema.methods.toJSON = function() {
  return {
    id: this._id,
    dateTime: this.dateTime,
    doneTime: this.doneTime,
    status: this.status,
    done: this.done
  };
};



TransactionSchema.statics.canChangeIngredients = function(pizzaId, cb) {
  this
  	.findOne({pizzas: {'$in': [pizzaId]}})
  	.exec(function(err, trans) {
  		
    	if (err) return cb(err);
    	
    	cb(null, trans.status == 'pedido')
  
  	});
};



var TTCSchema = new db.Schema({

  order_org: {type: String, enum: ['f2f','web','phn']},
  service: {type: String, enum: ['loc','out','dlv']}

});

TTCSchema.methods.toJSON = function (){
  return {
    id: this._id,
    order_org: this.order_org,
    service: this.service
  };

};

var Status = db.model('Status', TransStatusSchema );
var Transaction = db.model('Transaction', TransactionSchema);
var TTC = db.model('TTC', TTCSchema);
