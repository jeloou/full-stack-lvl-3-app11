var db = require('mongoose'),
    Transaction = db.model('Transaction');

module.exports = (function(app) {
  app.route('/transactions')
  //commented for testing with postman
      /*.all(function(req, res, next) {
      if (!req.trans) return res.status(401).end();
      next();
      })*/
    .post(function(req, res) {
      console.log( req.body);
      var trans;

      trans = {
      	total: req.body.total,
      	pizzas : req.body.pizzas.split(','),
      	client : req.body.client,
      	waiter : req.body.waiter,
      	ttc : req.body.ttc.split(','), // f2f,dlv
      	status: req.body.status
      };

      Transaction.add(trans, function(err, trans) {
	if (err) return res.status(400).json(err);
	res.json(trans);
      });

    })
    .get(function(req, res) {

    });

  app.route('/transactions/:id')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .get(function(req, res) {

    })
    .put(function(req, res) {

    })
    .delete(function(req, res) {

    });
});
