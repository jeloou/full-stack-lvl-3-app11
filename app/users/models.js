var db = require('mongoose'),
    crypto = require('crypto');

var Schema = new db.Schema({
  username: {type: String, lowercase: true, trim: true, default: ''},
  hash: {type: String, defualt: ''},
  salt: {type: String, defualt: ''},
  nombre: {type: String, lowercase: true, trim: true, default: ''},
  rut: {type: Number, required: true},
  direccion: {type: String, default: ''},
  telefono: {type: Number, required: true},
  role: {type: String, enum: ['admin','client','kitchen','delivery', 'waiter', 'teller']}
});

Schema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.genSalt();
    this.hash = this.genHash(password);
  })
  .get(function() {
    return this._password;
  });

Schema.methods.toJSON = function() {
  var res = {
    id: this._id,
    username: this.username,
    nombre: this.nombre,
    rut: this.rut,
    direccion: this.direccion,
    telefono: this.telefono
  };

  if (this.role !== 'client') {
    res.role = this.role;
  }

  return res;
};

Schema.methods.authenticate = function(password) {
  return this.genHash(password) === this.hash;
};

Schema.methods.genSalt = function() {
  return crypto
    .randomBytes(64)
    .toString('base64');
};

Schema.methods.genHash = function(password) {
  return crypto
    .pbkdf2Sync(password, this.salt, 10000, 128)
    .toString('base64');
};

Schema.statics.add = function(payload, fn) {
  var user;

  user = new(this)(payload);

  this
    .findOne({username: payload.username}, function(err, data){

      if(data){
	return fn({message: 'User Already Exists!'});
      }

      user.save(function(err) {
	if (err) throw err;
	fn(null, user);
      });

    })

};

var User = db.model('User', Schema);
