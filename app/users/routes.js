var db = require('mongoose'),
    User = db.model('User');

module.exports = (function(app) {
  app.get('/signup', function(req, res) {
    res.render('signup', {
      title: 'Pizzas - signup'
    });
  });

  app.post('/users', function(req, res) {
    console.log( req.body);
    var user;

    user = {
      username: req.body.email,
      hash: req.body.hash,
      salt: req.body.salt,
      nombre: req.body.nombre,
      rut: req.body.rut,
      direccion: req.body.direccion,
      telefono: req.body.telefono
    };

    User.add(user, function(err, user) {
      if (err) return res.status(400).json(err);
      res.json(user);
    });
  });

  app.get('/users/me', function(req, res) {

    console.log("req.user: " + req.user);

    User.get(req.user, function(err, user) {
      if (err) return res.status(401).json(err);
      res.json(user);
    });

  });

  app.get('/users/:id', function(req, res){

    var temp = {
      _id: req.params.id
    };

    User.get(temp, function(err, user){

      if(err) return res.status(401).json(err);
      res.json(user);

    });
  });
});


