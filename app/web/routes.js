module.exports = (function(app, passport) {
  app.get('/', function(req, res) {
    if (!req.user) return res.redirect('/login');

    res.render('index', {
      title: 'Pizzas'
    });
  });

  app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
  }));

  app.get('/login', function(req, res) {
    res.render('login', {
      title: 'Pizzas - login'
    });
  });
});
